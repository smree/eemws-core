#!/bin/sh
. ./commEnv.sh
if [ ! -d ../log ] ; then
        cd ..
        mkdir log
        cd bin
fi

# Do not include argument "-Dinteractive" in no-interactive environments  (ie daemons, background process, etc.)
# Set the property "-DRESET_CODE" to set the list by code to 0 (fresh start)
# Set the property "-RESET_DATE" to set the list by date to a specific date expressed in epoch format (ms from 1/1/1970)
# Set the property "-DLIST_BY_DATE" to list by server timestamp instead of code. 
# Set the property "-DMF_RETRY_ATTEMPTS" to the number of retry attempts of a get operation in case the operation fails.

java $MEM_ARGS $JAVA_OPTIONS $FILE_LOG es.ree.eemws.kit.folders.FolderManager
