/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */


package es.ree.eemws.core.utils.security;

import java.security.cert.CertificateException;

/**
 * Implements a wrapper for X509 certificate validation status.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */

public class X509ValidationStatus {

	/** Error message if the X509 certificate is not valid. */
	private String errorMessage;
	
	/** Cause of invalidation. */
	private CertificateException cause;
	
	/**
	 * Creates a new status with the given message and cause.
	 * @param errMsg Error message, can be <code>null</code> if the certificate is ok.
	 * @param cex Cause exception, can be <code>null</code> if the certificate is ok.
	 */
	public X509ValidationStatus(final String errMsg, final CertificateException cex) {
		errorMessage=errMsg;
		cause=cex;
	}
	
	/**
	 * Creates a new status as valid certificate.
	 */
	public X509ValidationStatus() {
		this(null, null);
	}

	/**
	 * Return if the cetificate was valid.
	 * @return <code>true</code> if the certificate is valid. <code>false</code> otherwise.
	 */
	public boolean isValid() {
		return cause == null;
	}
	
	/**
	 * Return the validation's error message.
	 * @return Validation's error message. Can be <code>null</code> if the status is ok.
	 */
	public String getErrorMessage() {
		return errorMessage;
	}
	
	/**
	 * Return the cause exception.
	 * @return Cause exception. Can be <code>null</code> if the status is ok.
	 */
	public CertificateException getCause() {
		return cause;
	}
}
