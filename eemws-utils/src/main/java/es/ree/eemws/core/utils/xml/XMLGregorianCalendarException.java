/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.core.utils.xml;

import javax.xml.datatype.DatatypeConfigurationException;

/**
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class XMLGregorianCalendarException extends RuntimeException {

	/** Serial version UID */
	private static final long serialVersionUID = 1735228512647202740L;
   
	/**
	 * Creates a new XMLGregorianCalendarException with the given message an cause.
	 * @param errMsg Error Message.
	 * @param e Cause.
	 */
	public XMLGregorianCalendarException(final String errMsg, final DatatypeConfigurationException e) {
		super(errMsg, e);
	}
}
