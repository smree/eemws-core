/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */
package es.ree.eemws.kit.gui.applications.browser;

import java.awt.Component;
import java.awt.event.MouseEvent;
import java.io.Serializable;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import es.ree.eemws.kit.gui.common.Constants;

/**
 * Table containing data to show (view model).
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class DataTable implements Serializable {

	/** Serial version UID. */
	private static final long serialVersionUID = -4817740893153694795L;

	/** Constant referring to 'Select All'. */
	public static final int SELECTION_ALL = 1;

	/** Constant referring to 'Clear selection'. */
	public static final int SELECTION_NONE = 2;

	/** Constant referring to 'Invert Selection'. */
	public static final int SELECTION_INVERT = 3;

	/** Scrollable container for table. */
	private JScrollPane pnlScrollableTableArea;

	/** List table. */
	private JTable tblListTable;

	/** Table model. */
	private ListTableModel tableModel;

	/** Reference to main window. */
	private Browser mainWindow;

	/** Status bar. */
	private StatusBar statusBar;

	/** Object for message request. */
	private GetMessageSender requestSend;

	/**
	 * Constructor. Initialize graphical elements on screen
	 *
	 * @param window Reference to main window.
	 */
	public DataTable(final Browser window) {

		mainWindow = window;
		statusBar = mainWindow.getStatusBar();
		requestSend = mainWindow.getRequestSend();

		tableModel = new ListTableModel();
		tblListTable = new JTable(tableModel);
		tblListTable.setAutoCreateRowSorter(true);
		tblListTable.getTableHeader().setFont(Constants.FONT_STYLE);
		tblListTable.setDefaultRenderer(Object.class, new TableStrippedCellRender());
		tblListTable.setDefaultRenderer(java.math.BigInteger.class, new TableStrippedCellRender());
		tblListTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		tblListTable.setFont(Constants.FONT_STYLE);
		tblListTable.addMouseListener(new java.awt.event.MouseAdapter() {
			@Override
			public void mouseClicked(final MouseEvent e) {
				/* Double click. */
				if (e.getClickCount() == 2) {
					requestSend.retrieve();
				}
			}
		});
		tblListTable.getSelectionModel().addListSelectionListener(e -> changeSelectionStatus());

		pnlScrollableTableArea = new JScrollPane();
		pnlScrollableTableArea.getViewport().add(tblListTable);

	}

	/**
	 * Enables / disables data table.
	 *
	 * @param value <code>true</code> Enable table. <code>false</code> disable.
	 */
	public void setEnabled(final boolean value) {
		tblListTable.setEnabled(value);
		pnlScrollableTableArea.setEnabled(value);
	}

	/**
	 * Returns model associated to table.
	 *
	 * @return Model associated to table.
	 */
	public ListTableModel getModel() {
		return tableModel;
	}

	/**
	 * Returns date selection menu.
	 *
	 * @return Actions menu.
	 */
	public JMenu getSelectionMenu() {

		/* Select All. */
		var miSelectAll = new JMenuItem();
		miSelectAll.setText(MessageCatalog.BROWSER_SELECT_ALL_MENU_ENTRY.getMessage());
		miSelectAll.setMnemonic(MessageCatalog.BROWSER_SELECT_ALL_MENU_ENTRY_HK.getChar());
		miSelectAll.addActionListener(e -> tableSelection(DataTable.SELECTION_ALL));

		/* Remove selecion. */
		var miSelectNone = new JMenuItem();
		miSelectNone.setText(MessageCatalog.BROWSER_SELECT_NONE_MENU_ENTRY.getMessage());
		miSelectNone.setMnemonic(MessageCatalog.BROWSER_SELECT_NONE_MENU_ENTRY_HK.getChar());
		miSelectNone.addActionListener(e -> tableSelection(DataTable.SELECTION_NONE));

		/* Invert selection. */
		var miSelectInvert = new JMenuItem();
		miSelectInvert.setText(MessageCatalog.BROWSER_SELECT_INVERT_MENU_ENTRY.getMessage());
		miSelectInvert.setMnemonic(MessageCatalog.BROWSER_SELECT_INVERT_MENU_ENTRY_HK.getChar());
		miSelectInvert.addActionListener(e -> tableSelection(DataTable.SELECTION_INVERT));

		/* Select option. */
		var mnSelectionMenu = new JMenu();
		mnSelectionMenu.setText(MessageCatalog.BROWSER_SELECT_MENU_ENTRY.getMessage());
		mnSelectionMenu.setMnemonic(MessageCatalog.BROWSER_SELECT_MENU_ENTRY_HK.getChar());
		mnSelectionMenu.add(miSelectAll);
		mnSelectionMenu.add(miSelectNone);
		mnSelectionMenu.add(miSelectInvert);

		return mnSelectionMenu;
	}

	/**
	 * Changes status bar text according to table's status (num rows, num of
	 * selected rows).
	 */
	private void changeSelectionStatus() {
		var totalRowNum = tableModel.getRowCount();
		var numSelectedRows = tblListTable.getSelectedRowCount();

		if (totalRowNum == 1) {
			if (numSelectedRows == 0) {
				statusBar.setStatus(MessageCatalog.BROWSER_STATUS_MESSAGE.getMessage());
			} else {
				statusBar.setStatus(MessageCatalog.BROWSER_STATUS_MESSAGE_SELECTED.getMessage());
			}
		} else if (numSelectedRows == 0) {
			statusBar.setStatus(MessageCatalog.BROWSER_STATUS_MESSAGES.getMessage(totalRowNum));
		} else {
			statusBar.setStatus(
			        MessageCatalog.BROWSER_STATUS_MESSAGES_SELECTED.getMessage(totalRowNum, numSelectedRows));
		}
	}

	/**
	 * Selects table elements according to the entered mode
	 * <li>mode=1: Select all elements in table.
	 * <li>mode=2: Clear current selection.
	 * <li>mode=3: Invert selection.
	 *
	 * @param mode Selection mode.
	 */
	private void tableSelection(final int mode) {
		if (mode == SELECTION_ALL) {
			tblListTable.selectAll();
		}

		if (mode == SELECTION_NONE) {
			tblListTable.clearSelection();
		}

		if (mode == SELECTION_INVERT) {
			var len = tblListTable.getRowCount();

			for (var cont = 0; cont < len; cont++) {
				if (tblListTable.isRowSelected(cont)) {
					tblListTable.removeRowSelectionInterval(cont, cont);
				} else {
					tblListTable.addRowSelectionInterval(cont, cont);
				}
			}
		}
	}

	/**
	 * Returns number of selected rows in table.
	 *
	 * Is necessary to perform a conversion from view to model to prevent problems
	 * related to order.
	 *
	 * @return Number of files selected on table.
	 */
	public int[] getSelectedRows() {
		var selectedRows = tblListTable.getSelectedRows();
		var len = selectedRows.length;
		var orderedSelectedRows = new int[len];
		for (var cont = 0; cont < len; cont++) {
			orderedSelectedRows[cont] = tblListTable.convertRowIndexToModel(selectedRows[cont]);
		}

		return orderedSelectedRows;
	}

	/**
	 * Gets index of the selected file. Is necessary to perform a conversion from
	 * view to model to prevent problems related to order.
	 *
	 * @return index of the selected file on table.
	 */
	public int getSelectedRow() {
		return tblListTable.convertRowIndexToModel(tblListTable.getSelectedRow());
	}

	/**
	 * Sets data to be shown on table.
	 *
	 * @param data Data to be shown on table.
	 */
	public void setData(final Object[][] data) {
		tableModel.setValues(data);
	}

	/**
	 * Indicates whether the table is empty.
	 *
	 * @return <code>true</code> If table is empty. <code>false</code> otherwise.
	 */
	public boolean isEmpty() {
		return tableModel.getRowCount() != 0;
	}

	/**
	 * Return scrollable panel containing message table.
	 *
	 * @return Scrollable panel containing message table.
	 */
	public Component getPanelScroll() {
		return pnlScrollableTableArea;
	}

	/**
	 * Adjusts the table size according to the values passed as parameters.
	 *
	 * @param width           Width of the main window.
	 * @param height          Height of the main window.
	 * @param isFilterVisible Indicates whether the data filter is visible.
	 */
	public void adjustTableSize(final int width, final int height, final boolean isFilterVisible) {
		if (isFilterVisible) {
			pnlScrollableTableArea.setBounds(2, 166, width - 30, height - 250);
		} else {
			pnlScrollableTableArea.setBounds(2, 6, width - 30, height - 90);
		}
		tblListTable.repaint();
	}

}
