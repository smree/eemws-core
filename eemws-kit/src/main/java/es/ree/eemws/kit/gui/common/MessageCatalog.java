/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.common;

import es.ree.eemws.kit.common.Messages;

/**
 * Common GUI message's codes.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public enum MessageCatalog {

	/** Error */
	MSG_ERROR_TITLE,

	/** Information. */
	MSG_INFO_TITLE,

	/** Confirm */
	MSG_CONFIRM_TITLE,

	/** Warning */
	MSG_WARNING_TITLE,

	/** Question. */
	MSG_QUESTION_TITLE,

	/** Log. */
	LOG_FRAME_TITLE,

	/** Exception details: */
	LOG_DETAIL_EXCEPTION,

	/** Clear. */
	LOG_CLEAR_MENU_ITEM,

	/** Clear hot key. */
	LOG_CLEAR_MENU_ITEM_HK,

	/** Show. */
	LOG_SHOW_MENU_ITEM,

	/**  Show hot key. */
	LOG_SHOW_MENU_ITEM_HK,

	/** Log. */
	LOG_MENU_ITEM,

	/** Menu log hot key. */
	LOG_MENU_ITEM_HK;

	/**
	 * Gets message text.
	 * @return Message text.
	 */
	public String getMessage(final Object... parameters) {
		return Messages.getString(name(), parameters);
	}

	/**
	 * Return the first chart of the message.
	 * @return First chart of the message.
	 */
	public char getChar() {
		return Messages.getString(name()).charAt(0);
	}
}


