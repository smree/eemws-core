/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */
package es.ree.eemws.kit.gui.applications.browser;

import java.awt.Component;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import es.ree.eemws.core.utils.iec61968100.EnumIntervalTimeType;
import es.ree.eemws.kit.gui.common.Constants;

/**
 * Graphical filter options.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class Filter implements Serializable {

	/** Serial version UID. */
	private static final long serialVersionUID = 1L;

	/** Date format for components related to date on filter. */
	public static final String DATE_FORMAT = "dd/MM/yyyy"; //$NON-NLS-1$

	/** Default value for code list. */
	private static final String ZERO_CODE = "0"; //$NON-NLS-1$

	/** Type of filter. */
	private JComboBox<String> cbFilterType = null;

	/** Label for {@link #txtCode}. */
	private JLabel lblCode = new JLabel();

	/** 'List' button. */
	private JButton btList = new JButton();

	/** 'Get' button. */
	private JButton btGet = new JButton();

	/** Date format. */
	private SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

	/** Label for {@link #txtStartDate}. */
	private JLabel lblStartDate = new JLabel();

	/** Start date. */
	private JFormattedTextField txtStartDate = new JFormattedTextField(sdf);

	/** Label for {@link #txtEndDate}. */
	private JLabel lblEndDate = new JLabel();

	/** End date. */
	private JFormattedTextField txtEndDate = new JFormattedTextField(sdf);

	/** Code. */
	private JTextField txtCode = new JTextField();

	/** ID. */
	private JTextField txtID = new JTextField();

	/** Message type. */
	private JTextField txtMessageType = new JTextField();

	/** Owner. */
	private JTextField txtOwner;

	/** Filter panel. */
	private JPanel pnlFilter = null;

	/** Reference to main window. */
	private Browser mainWindow;

	/**
	 * Constructor. Creates an instance of filter.
	 *
	 * @param mainW Reference to main class.
	 */
	public Filter(final Browser mainW) {
		mainWindow = mainW;
	}

	/**
	 * Sets current filtering type. Switch between code and date based filtering.
	 */
	private void switchFilterType() {

		var isDateType = cbFilterType.getSelectedIndex() != 0;

		lblStartDate.setVisible(isDateType);
		txtStartDate.setVisible(isDateType);
		lblEndDate.setVisible(isDateType);
		txtEndDate.setVisible(isDateType);
		lblCode.setVisible(!isDateType);
		txtCode.setVisible(!isDateType);
	}

	/**
	 * Returns filter panel with graphical components (textboxes, labels, etc.).
	 *
	 * @return Filter panel containing graphical elements.
	 */
	public JPanel getFilterCanvas() {

		var tipoFiltroLbl = new JLabel(MessageCatalog.BROWSER_FILTER_TYPE.getMessage(), SwingConstants.RIGHT);
		tipoFiltroLbl.setFont(Constants.FONT_STYLE);
		tipoFiltroLbl.setDisplayedMnemonic(MessageCatalog.BROWSER_FILTER_TYPE_HK.getChar());
		tipoFiltroLbl.setLabelFor(cbFilterType);
		tipoFiltroLbl.setBounds(16, 28, 75, 16);

		cbFilterType = new JComboBox<>();
		cbFilterType.setEditable(false);

		/* Must be added in the same order as in {@link ListMessages} */
		cbFilterType.addItem(MessageCatalog.BROWSER_FILTER_TYPE_CODE.getMessage());
		cbFilterType.addItem(MessageCatalog.BROWSER_FILTER_TYPE_SERVER.getMessage());
		cbFilterType.addItem(MessageCatalog.BROWSER_FILTER_TYPE_APPLICATION.getMessage());

		cbFilterType.setSelectedIndex(2);
		cbFilterType.setFont(Constants.FONT_STYLE);
		cbFilterType.setBounds(100, 28, 130, 19);
		cbFilterType.addActionListener(e -> switchFilterType());

		lblStartDate.setDisplayedMnemonic(MessageCatalog.BROWSER_FILTER_START_DATE_HK.getChar());
		lblStartDate.setLabelFor(txtStartDate);
		lblStartDate.setFont(Constants.FONT_STYLE);
		lblStartDate.setText(MessageCatalog.BROWSER_FILTER_START_DATE.getMessage());
		lblStartDate.setHorizontalAlignment(SwingConstants.RIGHT);
		lblStartDate.setBounds(16, 53, 75, 16);

		txtStartDate.setText(sdf.format(new Date()));
		txtStartDate.setFont(Constants.FONT_STYLE);
		txtStartDate.setFocusLostBehavior(JFormattedTextField.PERSIST);
		txtStartDate.setBounds(100, 53, 92, 19);

		lblEndDate.setDisplayedMnemonic(MessageCatalog.BROWSER_FILTER_END_DATE_HK.getChar());
		lblEndDate.setLabelFor(txtEndDate);
		lblEndDate.setFont(Constants.FONT_STYLE);
		lblEndDate.setText(MessageCatalog.BROWSER_FILTER_END_DATE.getMessage());
		lblEndDate.setHorizontalAlignment(SwingConstants.RIGHT);
		lblEndDate.setBounds(16, 78, 75, 16);

		txtEndDate.setText(sdf.format(new Date()));
		txtEndDate.setFocusLostBehavior(JFormattedTextField.PERSIST);
		txtEndDate.setBounds(100, 78, 92, 19);
		txtEndDate.setFont(Constants.FONT_STYLE);

		lblCode.setDisplayedMnemonic(MessageCatalog.BROWSER_FILTER_CODE_HK.getChar());
		lblCode.setLabelFor(txtCode);
		lblCode.setFont(Constants.FONT_STYLE);
		lblCode.setText(MessageCatalog.BROWSER_FILTER_CODE.getMessage());
		lblCode.setHorizontalAlignment(SwingConstants.RIGHT);
		lblCode.setBounds(16, 53, 75, 16);
		lblCode.setVisible(false);

		txtCode.setText(ZERO_CODE);
		txtCode.setBounds(100, 53, 92, 19);
		txtCode.setVisible(false);
		txtCode.setFont(Constants.FONT_STYLE);

		var lblID = new JLabel(MessageCatalog.BROWSER_FILTER_ID.getMessage(), SwingConstants.RIGHT);
		lblID.setLabelFor(txtID);
		lblID.setDisplayedMnemonic(MessageCatalog.BROWSER_FILTER_ID_HK.getChar());
		lblID.setBounds(200, 28, 130, 16);
		lblID.setFont(Constants.FONT_STYLE);

		txtID.setText(""); //$NON-NLS-1$
		txtID.setBounds(340, 28, 130, 19);
		txtID.setFont(Constants.FONT_STYLE);

		var lblMessageType = new JLabel(MessageCatalog.BROWSER_FILTER_MSG_TYPE.getMessage(), SwingConstants.RIGHT);
		lblMessageType.setDisplayedMnemonic(MessageCatalog.BROWSER_FILTER_MSG_TYPE_HK.getChar());
		lblMessageType.setLabelFor(txtMessageType);
		lblMessageType.setBounds(200, 53, 130, 16);
		lblMessageType.setFont(Constants.FONT_STYLE);

		txtMessageType.setText(""); //$NON-NLS-1$
		txtMessageType.setBounds(340, 53, 130, 19);
		txtMessageType.setFont(Constants.FONT_STYLE);

		var lblOwner = new JLabel(MessageCatalog.BROWSER_FILTER_OWNER.getMessage(), SwingConstants.RIGHT);
		lblOwner.setLabelFor(txtOwner);
		lblOwner.setDisplayedMnemonic(MessageCatalog.BROWSER_FILTER_OWNER_HK.getChar());
		lblOwner.setBounds(200, 78, 130, 16);
		lblOwner.setFont(Constants.FONT_STYLE);

		txtOwner = new JTextField(""); //$NON-NLS-1$
		txtOwner.setBounds(340, 78, 130, 19);
		txtOwner.setFont(Constants.FONT_STYLE);

		btList.setBounds(90, 112, 121, 26);
		btList.setMnemonic(MessageCatalog.BROWSER_FILTER_BROWSER_BUTTON_HK.getChar());
		btList.setText(MessageCatalog.BROWSER_FILTER_BROWSER_BUTTON.getMessage());
		btList.setFont(Constants.FONT_STYLE);
		btList.addActionListener(e -> mainWindow.getListSend().retrieveList());

		btGet.setBounds(251, 112, 121, 26);
		btGet.setMnemonic(MessageCatalog.BROWSER_FILTER_GET_BUTTON_HK.getChar());
		btGet.setText(MessageCatalog.BROWSER_FILTER_GET_BUTTON.getMessage());
		btGet.setFont(Constants.FONT_STYLE);
		btGet.addActionListener(e -> mainWindow.getRequestSend().retrieve());

		pnlFilter = new JPanel();
		pnlFilter.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(),
		        MessageCatalog.BROWSER_FILTER_LEGEND.getMessage()));
		pnlFilter.setLayout(null);

		pnlFilter.add(tipoFiltroLbl);
		pnlFilter.add(cbFilterType);
		pnlFilter.add(lblStartDate);
		pnlFilter.add(txtStartDate);
		pnlFilter.add(txtEndDate);
		pnlFilter.add(lblEndDate);
		pnlFilter.add(lblCode);
		pnlFilter.add(txtCode);
		pnlFilter.add(lblID);
		pnlFilter.add(txtID);
		pnlFilter.add(btList);
		pnlFilter.add(btGet);
		pnlFilter.add(lblMessageType);
		pnlFilter.add(txtMessageType);
		pnlFilter.add(lblOwner);
		pnlFilter.add(txtOwner);
		pnlFilter.setVisible(true);

		pnlFilter.setBounds(0, 0, 500, 160);

		return pnlFilter;
	}

	/**
	 * Resizes filter box size according to the value passed as parameter.
	 *
	 * @param width Width of the parent window.
	 */
	public void setSize(final int width) {
		pnlFilter.setBounds(0, 0, width - 20, 160);
	}

	/**
	 * Enables / disables graphical values.
	 *
	 * @param activeValue <code>true</code> Enable. <code>false</code> disable.
	 */
	public void enable(final boolean activeValue) {
		var component = pnlFilter.getComponents();
		for (Component comp : component) {
			comp.setEnabled(activeValue);
		}
	}

	/**
	 * Builts <code>FilterData</code> object value with the current filter options.
	 *
	 * @return Object with the current filter options.
	 */
	public FilterData getFilterData() {
		var df = new FilterData();

		var listType = cbFilterType.getSelectedIndex();

		if (listType == 0) {
			df.setStartDate(null);
			df.setEndDate(null);
			df.setCode(txtCode.getText().trim());
		} else {

			if (listType == 2) {
				df.setMsgInterval(EnumIntervalTimeType.APPLICATION);
			} else {
				df.setMsgInterval(EnumIntervalTimeType.SERVER);
			}
			df.setStartDate(txtStartDate.getText());
			df.setEndDate(txtEndDate.getText());
		}

		df.setType(txtMessageType.getText());
		df.setMessageID(txtID.getText());
		df.setOwner(txtOwner.getText());

		return df;
	}

	/**
	 * Shows / hides filters according to the parameter.
	 *
	 * @param visibility <code>true</code> to show filter. <code>false</code> to
	 *                   hide it.
	 */
	private void setVisible(final boolean visibility) {
		pnlFilter.setVisible(visibility);
		mainWindow.modifySize();
	}

	/**
	 * Indicates whether filter is visible (useful for adjust size of other
	 * elements) .
	 *
	 * @return <code>true</code> if the filter is visible <code>false</code>
	 *         otherwise.
	 */
	public boolean isVisible() {
		return pnlFilter.isVisible();
	}

	/**
	 * Returns menu containing filtering options.
	 *
	 * @param mnView Menu containing filtering options.
	 */
	public void getMenu(final JMenu mnView) {

		final var miShowFilter = new JCheckBoxMenuItem(
		        MessageCatalog.BROWSER_FILTER_SHOW_FILTER_MENU_ENTRY.getMessage());
		miShowFilter.setMnemonic(MessageCatalog.BROWSER_FILTER_SHOW_FILTER_MENU_ENTRY_HK.getChar());
		miShowFilter.addActionListener(e -> setVisible(miShowFilter.isSelected()));
		miShowFilter.setSelected(true);

		mnView.add(miShowFilter);
	}

}
