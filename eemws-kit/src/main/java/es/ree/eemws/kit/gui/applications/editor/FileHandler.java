/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.gui.applications.editor;

import java.awt.Component;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetAdapter;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.InputEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;

import es.ree.eemws.core.utils.file.FileUtil;
import es.ree.eemws.kit.gui.common.Logger;

/**
 * Processing of File actions.
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public final class FileHandler extends DropTargetAdapter {

	/**
	 * Character to notify in the application title that document has been modified.
	 */
	public static final String MODIFIED_FILE_CHAR = "*"; //$NON-NLS-1$

	/** File name start character token. */
	private static final String TITLE_START = "["; //$NON-NLS-1$

	/** File name end character token. */
	private static final String TITLE_END = "]"; //$NON-NLS-1$

	/** Editable text handler. */
	private DocumentHandle documentHandle = null;

	/** File menu. */
	private JMenu fileMenu = new JMenu();

	/** Button bar. */
	private JToolBar toolBar = new JToolBar();

	/** Log window. */
	private Logger log;

	/** Main window. */
	private Editor mainWindow = null;

	/** File currently edited. */
	private File currentFile = null;

	/**
	 * Creates a new instance of the File handler.
	 *
	 * @param window Reference to main window.
	 */
	public FileHandler(final Editor window) {

		mainWindow = window;
		documentHandle = mainWindow.getDocumentHandle();
		log = mainWindow.getLogHandle().getLog();

		/* Enables the editor open files by dropping items. */
		new DropTarget(documentHandle.getDocument(), this);
		newFile();
	}

	/**
	 * Gets file menu.
	 *
	 * @return File Menu items for the main menu.
	 */
	public JMenu getMenu() {

		var newFileMenuItem = new JMenuItem(MessageCatalog.EDITOR_MENU_ITEM_NEW.getMessage(),
		        new ImageIcon(getClass().getResource(es.ree.eemws.kit.gui.common.Constants.ICON_NEW)));
		newFileMenuItem.setMnemonic(MessageCatalog.EDITOR_MENU_ITEM_NEW_HK.getChar());
		newFileMenuItem.addActionListener(e -> newFile());

		var openFileMenuItem = new JMenuItem(MessageCatalog.EDITOR_MENU_ITEM_OPEN.getMessage(),
		        new ImageIcon(getClass().getResource(es.ree.eemws.kit.gui.common.Constants.ICON_OPEN)));
		openFileMenuItem.setMnemonic(MessageCatalog.EDITOR_MENU_ITEM_OPEN_HK.getChar());
		openFileMenuItem.addActionListener(e -> openFile());

		var saveMenuItem = new JMenuItem(MessageCatalog.EDITOR_MENU_ITEM_SAVE.getMessage(),
		        new ImageIcon(getClass().getResource(es.ree.eemws.kit.gui.common.Constants.ICON_SAVE)));
		saveMenuItem.setMnemonic(MessageCatalog.EDITOR_MENU_ITEM_SAVE_HK.getChar());
		saveMenuItem.setAccelerator(KeyStroke.getKeyStroke('S', InputEvent.CTRL_DOWN_MASK));
		saveMenuItem.addActionListener(e -> saveFile());

		var saveAsMenuItem = new JMenuItem(MessageCatalog.EDITOR_MENU_ITEM_SAVE_AS.getMessage(),
		        new ImageIcon(getClass().getResource(es.ree.eemws.kit.gui.common.Constants.ICON_SAVE_AS)));
		saveAsMenuItem.setMnemonic(MessageCatalog.EDITOR_MENU_ITEM_SAVE_AS_HK.getChar());
		saveAsMenuItem.addActionListener(e -> saveFileAs());

		var menuItemExit = new JMenuItem();
		menuItemExit.setText(MessageCatalog.EDITOR_MENU_ITEM_EXIT.getMessage());
		menuItemExit.setMnemonic(MessageCatalog.EDITOR_MENU_ITEM_EXIT.getChar());
		menuItemExit.addActionListener(e -> exitProgram());

		fileMenu.setText(MessageCatalog.EDITOR_MENU_ITEM_FILE.getMessage());
		fileMenu.setMnemonic(MessageCatalog.EDITOR_MENU_ITEM_FILE.getChar());
		fileMenu.add(newFileMenuItem);
		fileMenu.add(openFileMenuItem);
		fileMenu.add(saveMenuItem);
		fileMenu.add(saveAsMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(menuItemExit);

		return fileMenu;
	}

	/**
	 * Returns button bar related to File options.
	 *
	 * @return Option bar related to File options.
	 */
	public JToolBar getButtonBar() {

		toolBar.setFloatable(true);

		var newDocumentButton = new JButton();
		newDocumentButton
		        .setIcon(new ImageIcon(getClass().getResource(es.ree.eemws.kit.gui.common.Constants.ICON_NEW)));
		newDocumentButton.setToolTipText(MessageCatalog.EDITOR_MENU_ITEM_NEW.getMessage());
		newDocumentButton.setBorderPainted(false);
		newDocumentButton.addActionListener(e -> newFile());

		var openFileButton = new JButton();
		openFileButton.setToolTipText(MessageCatalog.EDITOR_MENU_ITEM_OPEN.getMessage());
		openFileButton.setBorderPainted(false);
		openFileButton.setIcon(new ImageIcon(getClass().getResource(es.ree.eemws.kit.gui.common.Constants.ICON_OPEN)));
		openFileButton.addActionListener(e -> openFile());

		var saveFileButton = new JButton();
		saveFileButton.setIcon(new ImageIcon(getClass().getResource(es.ree.eemws.kit.gui.common.Constants.ICON_SAVE)));
		saveFileButton.setToolTipText(MessageCatalog.EDITOR_MENU_ITEM_SAVE.getMessage());
		saveFileButton.setBorderPainted(false);
		saveFileButton.addActionListener(e -> saveFile());

		toolBar.add(newDocumentButton, null);
		toolBar.add(openFileButton, null);
		toolBar.add(saveFileButton, null);
		return toolBar;
	}

	/**
	 * Opens a file in edit area.
	 *
	 * @param file File to be opened.
	 */
	private void openFile(final File file) {

		if (file.exists()) {
			try {

				documentHandle.openIrreversible(new StringBuilder(FileUtil.readUTF8(file.getAbsolutePath())));
				log.logMessage(MessageCatalog.EDITOR_OPENING_FILE.getMessage(file.getAbsolutePath()));
				mainWindow.setTitle(TITLE_START + file.getName() + TITLE_END);
				currentFile = file;

			} catch (IOException ioe) {
				var errMsg = MessageCatalog.EDITOR_CANNOT_OPEN_FILE.getMessage(file.getAbsolutePath(),
				        ioe.getMessage());
				JOptionPane.showMessageDialog(mainWindow, errMsg,
				        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_ERROR_TITLE.getMessage(),
				        JOptionPane.ERROR_MESSAGE);
				log.logMessage(errMsg);
			}
		}
	}

	/**
	 * Opens a file in editor.
	 */
	private void openFile() {

		if (hasUserPermission()) {

			var fileChooser = new JFileChooser();
			var returnVal = fileChooser.showOpenDialog(mainWindow);
			if (returnVal == JFileChooser.APPROVE_OPTION) {

				openFile(fileChooser.getSelectedFile());
			}
		}
	}

	/**
	 * Saves content area in file, using the already existent file name.
	 */
	private void saveFile() {

		if (currentFile == null) {
			saveFileAs();

		} else {
			saveFile(currentFile);
		}
	}

	/**
	 * Saves Text area content in file.
	 */
	private void saveFileAs() {

		if (documentHandle.isEmpty()) {

			log.logMessage(MessageCatalog.EDITOR_NOTHING_TO_SAVE.getMessage());
			JOptionPane.showMessageDialog(mainWindow, MessageCatalog.EDITOR_NOTHING_TO_SAVE.getMessage(),
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_WARNING_TITLE.getMessage(),
			        JOptionPane.INFORMATION_MESSAGE);

		} else {

			var fileNameFromTitle = mainWindow.getTitle();
			fileNameFromTitle = fileNameFromTitle.substring(fileNameFromTitle.indexOf(TITLE_START) + 1,
			        fileNameFromTitle.indexOf(TITLE_END));

			var fileChooser = new JFileChooser();
			fileChooser.setSelectedFile(new File(fileNameFromTitle));
			var returnVal = fileChooser.showSaveDialog(mainWindow);

			if (returnVal == JFileChooser.APPROVE_OPTION) {

				var file = fileChooser.getSelectedFile();
				if (file.exists()) {

					var resp = JOptionPane.showConfirmDialog(mainWindow,
					        MessageCatalog.EDITOR_SAVE_FILE_ALREADY_EXISTS.getMessage(file.getName()),
					        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_WARNING_TITLE.getMessage(),
					        JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

					if (resp != JOptionPane.OK_OPTION) {

						return;
					}

					log.logMessage(MessageCatalog.EDITOR_SAVE_FILE_OVERWRITTEN.getMessage(file.getAbsolutePath()));
				}

				saveFile(file);
			}
		}
	}

	/**
	 * Saves a file.
	 *
	 * @param file File.
	 */
	private void saveFile(final File file) {

		try {

			var contenido = documentHandle.getPlainText();
			FileUtil.writeUTF8(file.getAbsolutePath(), contenido);
			log.logMessage(MessageCatalog.EDITOR_SAVE_FILE_SAVED.getMessage(file.getAbsolutePath()));
			currentFile = file;
			mainWindow.setTitle(TITLE_START + file.getName() + TITLE_END);

		} catch (IOException ioe) {

			var errMsg = MessageCatalog.EDITOR_UNABLE_TO_SAVE.getMessage(file.getAbsolutePath());

			JOptionPane.showMessageDialog(mainWindow, errMsg,
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_ERROR_TITLE.getMessage(), JOptionPane.ERROR_MESSAGE);

			log.logException(errMsg, ioe);
		}
	}

	/**
	 * CreateS a new file.
	 */
	private void newFile() {

		if (hasUserPermission()) {
			documentHandle.openIrreversible(new StringBuilder("")); //$NON-NLS-1$
			mainWindow.setTitle(TITLE_START + MessageCatalog.EDITOR_NEW_FILE_TITLE.getMessage() + TITLE_END);
			log.logMessage(MessageCatalog.EDITOR_NEW_FILE_TITLE.getMessage());
			currentFile = null;
		}
	}

	/**
	 * In case the message has been edited, application will ask user for
	 * confirmation before overwriting content (open, new, drop).
	 *
	 * @return <code>true</code> If changes are ignored. <code>false</code>
	 *         otherwise.
	 */
	private boolean hasUserPermission() {

		var permited = true;

		var title = mainWindow.getTitle();

		if (title.indexOf(MODIFIED_FILE_CHAR) != -1) {

			var fileName = title.substring(0, title.indexOf(MODIFIED_FILE_CHAR));
			fileName = fileName.substring(fileName.indexOf(TITLE_START) + 1, fileName.indexOf(TITLE_END));

			var answer = JOptionPane.showConfirmDialog(mainWindow,
			        MessageCatalog.EDITOR_LOSE_CHANGES.getMessage(fileName),
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_WARNING_TITLE.getMessage(),
			        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

			permited = (answer == JOptionPane.OK_OPTION);

		}

		return permited;
	}

	/**
	 * Method invoked before exit.
	 */
	public void exitProgram() {

		if (hasUserPermission()) {
			var res = JOptionPane.showConfirmDialog(mainWindow, MessageCatalog.EDITOR_EXIT_APPLICATION.getMessage(),
			        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_QUESTION_TITLE.getMessage(),
			        JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);

			if (res == JOptionPane.OK_OPTION) {
				System.exit(0); // NOSONAR We want to force application to exit.
			}
		}
	}

	/**
	 * Triggered when a link to a file (or any object else) is dropped on editor.
	 *
	 * @param eventoDrop Event triggered containing data related to dropped object.
	 */
	@Override
	@SuppressWarnings({ "rawtypes" })
	public void drop(final DropTargetDropEvent eventoDrop) {

		if (eventoDrop.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {

			try {

				eventoDrop.acceptDrop(DnDConstants.ACTION_LINK);
				var obj = (List) eventoDrop.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
				var file = (File) obj.get(0);
				if (!file.isDirectory()) {

					if (hasUserPermission()) {
						openFile(file);
					}

				} else {

					JOptionPane.showMessageDialog(mainWindow, MessageCatalog.EDITOR_CANNOT_LOAD_FOLDER.getMessage(),
					        es.ree.eemws.kit.gui.common.MessageCatalog.MSG_ERROR_TITLE.getMessage(),
					        JOptionPane.ERROR_MESSAGE);
					eventoDrop.dropComplete(false);
				}

			} catch (UnsupportedFlavorException | IOException e) {

				eventoDrop.dropComplete(false);
			}

		} else {

			eventoDrop.rejectDrop();
		}

	}

	/**
	 * Enables / disables graphic values.
	 *
	 * @param activeValue <code>true</code> Enable. <code>false</code> Disable.
	 */
	public void enable(final boolean activeValue) {

		for (Component comp : toolBar.getComponents()) {
			comp.setEnabled(activeValue);
		}

		for (Component comp : fileMenu.getMenuComponents()) {
			comp.setEnabled(activeValue);
		}
	}
}
