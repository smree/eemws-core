/*
 * Copyright 2024 Redeia.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation, version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTIBIILTY or FITNESS FOR A PARTICULAR PURPOSE. See GNU Lesser General
 * Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, see
 * http://www.gnu.org/licenses/.
 *
 * Any redistribution and/or modification of this program has to make
 * reference to Redeia as the copyright owner of the program.
 */

package es.ree.eemws.kit.folders;

import es.ree.eemws.kit.common.Messages;

/**
 * Message codes for Magic Folder
 *
 * @author Redeia.
 * @version 2.1 01/01/2024
 */
public enum MessageCatalog {

	/** [OUTPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Retrieved file already exists! {1} (won't be overwritten)  */
	MF_RETRIEVED_MESSAGE_ALREADY_EXISTS,

	/** Neither {0} nor {1} are set, Magic Folder won't work. */
	MF_UNABLE_TO_START,

	/** Host configuration {0} has an incorrect format (should be: 'host:port'). */
	MF_INVALID_MEMBER_URL,

	/** Port {0} must be a number. */
	MF_INVALID_MEMBER_PORT,

	/** {0} value ({1}) must be a number. */
	MF_INVALID_NUMBER,

	/** {0} value {1} is not a valid URL. */
	MF_INVALID_SET_URL,

	/** {0} value ({1}) has incorrect characters . */
	MF_INVALID_ID,

	/** {0} value ({1}) is too long (max length, /** {2}) */
	MF_INVALID_ID_LENGTH,

	/** {0} value ({1}) is not an existing folder. */
	MF_INVALID_FOLDER,

	/** {0} for key value {1} must be greater than {2} */
	MF_VALUE_TOO_SMALL,

	/** Backup task cannot be performed. */
	MF_UNABLE_TO_BACKUP,

	/** Unable to delete file {0} */
	MF_UNABLE_TO_DELETE

	/** Exit  */,
	MF_MENU_ITEM_EXIT,

	/** Exit hot key  */
	MF_MENU_ITEM_EXIT_HOT_KEY,

	/** Magic Folder */
	MF_STATUS_IDLE,

	/** Set {0}:  */
	MF_SET_NUM,

	/** Processing files...  */
	MF_STATUS_BUSY,

	/** Do you want to exit the application? */
	MF_EXIT_APPLICATION,

	/** Exit */
	MF_EXIT_APPLICATION_TITLE,

	/** There is an instance of the application already running. */
	MF_ALREADY_RUNNING,

	/** Error. */
	MF_TITLE_ERROR,

	/** Application running.... */
	MF_RUNNING,

	/** Cannot retrieve / register the remote references {0}. */
	MF_CANNOT_REACH_REFERENCES,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Cannot create fault from exception. Check stack trace for details. */
	MF_CANNOT_CREATE_FAULT_MSG,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Cannot create request for file {1}. */
	MF_RETURNS_ERROR,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Server has returned fault for file {1}. */
	MF_SERVER_RETURNS_FAULT,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Server has returned error for file {1}. */
	MF_SERVER_RETURNS_ERROR,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Cannot read the file {1}. */
	MF_CANNOT_READ_FILE,

	/** Unexpected error!. */
	MF_UNEXPECTED_ERROR,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Unexpected error!. */
	MF_UNEXPECTED_ERROR_I,

	/** [OUTPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Unexpected error!. */
	MF_UNEXPECTED_ERROR_O,

	/** [OUTPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Memory error! Check last operation. */
	MF_OOM_ERROR_O,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Input folder: {1} */
	MF_CONFIG_INPUT_FOLDER,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Response folder: {1} */
	MF_CONFIG_ACK_FOLDER,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] This folder contains BINARY files. */
	MF_CONFIG_BINARY_FOLDER,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] This folder contains XML files. */
	MF_CONFIG_XML_FOLDER,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Ok response folder: {1} */
	MF_CONFIG_ACK_OK_FOLDER,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Failed response folder: {1} */
	MF_CONFIG_ACK_FAILED_FOLDER,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] The following command line will be executed for each OK message: {1} */
	MF_CONFIG_CMD_ACK_OK,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] The following command line will be executed for each FAILED message: {1} */
	MF_CONFIG_CMD_ACK_FAILED,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Processed folder: {1} */
	MF_CONFIG_PROCESSED_FOLDER,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Delay time between detections: {1} ms. */
	MF_CONFIG_DELAY_TIME_I,

	/** [OUTPUT{0}] Delay time between detections: {1} ms. */
	MF_CONFIG_DELAY_TIME_O,

	/** [OUTPUT{0}] Delay time between detections has been overridden to {1} ms because the list will be made by date instead of code. */
	MF_CONFIG_DELAY_TIME_O_DATE,

	/** [OUTPUT{0}] Will continue listing using code {1} */
	MF_CONFIG_LST_CODE,

	/** [OUTPUT{0}] Will continue listing using date {1, date,dd-MM-yyyy HH:mm:ss.SSS} */
	MF_CONFIG_LST_DATE_DEFAULT_VALUE,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] URL {1} */
	MF_CONFIG_URL_I,

	/** [OUTPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] URL {1} */
	MF_CONFIG_URL_O,

	/** [OUTPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] File name extension: {1} */
	MF_FILE_NAME_EXTENSION,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Sending file {1}... */
	MF_SENDING_MESSAGE,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] File {1} sent. */
	MF_SENT_MESSAGE,

	/** [OUTPUT{0}] Retrieving message with code, /** {1} identification, /** {2} and version , /**  {3} */
	MF_RETRIEVING_MESSAGE,

	/** [OUTPUT{0}] Retrieving message with code, /** {1} identification, /** {2} */
	MF_RETRIEVING_MESSAGE_WO_VERSION,

	/** [OUTPUT{0}] Message with code, /** {1} identification, /** {2} and version , /**  {3} retrieved. */
	MF_RETRIEVED_MESSAGE,

	/** [OUTPUT{0}] Message with code, /** {1} identification, /** {2} retrieved. */
	MF_RETRIEVED_MESSAGE_WO_VERSION,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Unable to write the file {1} in the processed folder {2} */
	MF_SAVING_PROCESS_FOLDER,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Unable to write the response of file {1} in the response folder {2} */
	MF_SAVING_ACK_FOLDER,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Unable to write the response of file {1} in the response OK folder {2} */
	MF_SAVING_ACK_OK_FOLDER,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Unable to write the response of file {1} in the response FAILED folder {2} */
	MF_SAVING_ACK_FAILED_FOLDER,

	/** [INPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Unable to delete the file {1}. */
	MF_UNABLE_TO_DELETE_INPUT_FILE,

	/** [LOCK] Cannot create a registry, probably already exists one. */
	MF_UNABLE_TO_CREATE_REGISTRY,

	/** [LOCK] Cannot create a registry, check host and port values in the configuration file. URL, /** {0} */
	MF_INVALID_HOST_PORT,

	/** [LOCK] Cannot create a registry, check host and port values in the configuration file. URL, /** {0} */
	MF_INVALID_MEMBER_CONFIGURATION,

	/** [LOCK] Member {0} is not available yet...(waiting for member availability) */
	MF_MEMBER_NOT_AVAILABLE_YET,

	/** [LOCK] Member {0} is not available. */
	MF_MEMBER_NOT_AVAILABLE,

	/** [LOCK] Cannot subscribe to member {0}. */
	MF_CANNOT_SUSCRIBE,

	/** [LOCK] Member {0} does not respond, was removed from the group. */
	MF_MEMBER_GONE,

	/** [LOCK] Invalid url subscription received {0}. Check configuration files. */
	MF_INVALID_URL_RECEIVED,

	/** [LOCK] Received an url {0} that does not response. */
	MF_URL_NOT_BOUND,

	/** [LOCK] Running as standalone application (no communication with other machines) */
	MF_STAND_ALONE,

	/** [LOCK] Listening others members notifications with URL {0} */
	MF_GROUP_LISTEINGN_URL,

	/** [LOCK] Getting {0} remote reference/s. */
	MF_SEARCHING_MEMBERS,

	/** [LOCK] Connecting with member {0}. */
	MF_CONNECTING_WITH_MEMBER,

	/** [LOCK] Received new member subscription with url {0}. */
	MF_NEW_MEMBER,

	/** [LOCK] Received member update subscription with url {0}. */
	MF_UPDATE_MEMBER,

	/** [OUTPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Output folder: {1} */
	MF_CONFIG_OUTPUT_FOLDER,

	/** [OUTPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] All messages will be copied in the output folder. */
	MF_CONFIG_LST_MESSAGES_TYPE_ALL,

	/** [OUTPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] The following messages type will be copied in the output folder: {1} */
	MF_CONFIG_LST_MESSAGES_TYPE,

	/** [OUTPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] No program will be executed after message retrieval. */
	MF_NO_PROGRAM,

	/** [OUTPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Following command line will be executed for each message: {1} */
	MF_PROGRAM,

	/** [OUTPUT{0}] Cannot retrieve message with code, /** {1} identification, /** {2} and version, /** {3} */
	MF_UNABLE_TO_GET,

	/** [OUTPUT{0}] Cannot retrieve message with code, /** {1} identification, /** {2} */
	MF_UNABLE_TO_GET_WO_VERSION,

	/** [OUTPUT{0}] Cannot save message with code, /** {1} identification, /** {2} and version, /** {3} */
	MF_UNABLE_TO_SAVE,

	/** [OUTPUT{0}] Cannot save message with code, /** {1} identification, /** {2} */
	MF_UNABLE_TO_SAVE_WO_VERSION,

	/**  [OUTPUT{0}] The server has returned an error instead of the message with code , /** {1} identification, /** {2} y version, /** {3}. Retring... */
	MF_RETRY_GET,

	/**  [OUTPUT{0}] The server has returned an error instead of the message with code, /** {1} identification, /** {2}. Retring... */
	MF_RETRY_GET_WO_VERSION,

	/** [OUTPUT{0}] Unable to get messages list. */
	MF_UNABLE_TO_LIST,

	/** [RUN] Program execution {0} failed! */
	MF_RUN_ERROR,

	/** [RUN] Running: {0} */
	MF_RUN_INFO,

	/** The file {0} has been succesfully renamed to {1} */
	MF_FILE_RENAMED,

	/** Unable to rename {0} to {1}. Is there other process blocking the file?. Retring... */
	MF_FILE_CANNOT_BE_RENAMED_RETRYING,

	/** Unable to rename {0} to {1} after severals attempts. Giving up. */
	MF_FILE_CANNOT_BE_RENAMED_GIVING_UP,

	/** The regular expression {1} set in {0} is not valid. */
	MF_INVALID_REGEXP,

	/** [OUTPUT{0}] The list code has been set to 0. */
	MF_CONFIG_LST_CODE_RESET,

	/** [OUTPUT{0}] The inital list date has been set to {1, date,dd-MM-yyyy HH:mm:ss.SSS}. */
	MF_CONFIG_LST_DATE_RESET,

	/** [OUTPUT{0}] The given value for RESET_DATE must be a numeric value(was provided: {1}) */
	MF_CONFIG_LST_DATE_INVALID_VALUE,

	/** [OUTPUT{0}] List by date will use the default value: {1, date,dd-MM-yyyy HH:mm:ss.SSS}.  */
	MF_CONFIG_LST_DATE,

	/** [OUTPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] No filter by message identification is set. */
	MF_CONFIG_LST_PATTERNS_ALL,

	/** [OUTPUT{0,choice,0#|1#-{0}|1&lt;-{0}}] Messages which identification fits at least one of these expressions will be saved.: {1} */
	MF_CONFIG_LST_PATTERNS,

	/** [OUTPUT{0}] List loop by date [{1, date,dd-MM-yyyy HH:mm:ss.SSS} - {2, date,dd-MM-yyyy HH:mm:ss.SSS}] has retrieved {3} files. */
	MF_DATE_LOG;

	/**
	 * Gets message text.
	 * @return Message text.
	 */
	public String getMessage(final Object... parameters) {
		return Messages.getString(name(), parameters);
	}

	/**
	 * Return the first chart of the message.
	 * @return First chart of the message.
	 */
	public char getChar() {
		return Messages.getString(name()).charAt(0);
	}



}